#![allow(dead_code, unused_variables)]


use std::env;
use std::fs;

pub mod user {
    use std::env;

    pub fn get_command() {
        let args = env::args().collect::<Vec<String>>();
            if args.len() < 2 { let command = String::from("add"); }
            else { let command = args[1].clone();
        }

    // need to get `command: String` into this scope

    }
}

fn main() {
    let arguments = env::args().collect::<Vec<String>>();
    if arguments.len() < 2 {
        println!("\nProvide a command:\n\n\tstash add \"some string\"\n\tstash show")
        } else {
            let command = &arguments[1];

        if command == "show" {
            show().expect("There is no stash file.");
        } else if  command == "add" {
            let name_me = arguments[2].clone();
            add(name_me).expect("What went wrong?");
        } else {
            println!("That's not a valid command.");
        }
    }
}

fn show() -> std::io::Result<()> {
    let file = fs::read_to_string("stash.txt")?;
    println!("\n{}", file);
    Ok(())
}

fn add(mut addition: String) -> std::io::Result<()> {
    let file = fs::read_to_string("stash.txt")?;
    addition.push_str("\n");
    addition.push_str(&file);
    fs::write("stash.txt", addition.as_str())?;
    let file = fs::read_to_string("stash.txt")?;
    println!("\n{}", file);
    Ok(())
}
